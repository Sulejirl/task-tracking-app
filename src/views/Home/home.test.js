import { render, screen,fireEvent } from "@testing-library/react";
import Home from "./index";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { TaskProvider } from "contexts/taskContext";

test("Renders App", () => {
    const data = {
        createdAt: new Date(),
        createdBy: "Mehmet",
        description: "Lorem Ipsum",
        history: [
          {
            changedBy: "Mehmet",
            date: new Date(),
            newValue: "Neuer",
            oldValue: undefined,
            type: "ASSIGNEE_CHANGED",
          },
        ],
        id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
        status: "ToDo",
        title: "title",
      };
  render(
    <TaskProvider  testValue= {[data,data]}>
      <Home />
    </TaskProvider>
  );
  const home = screen.getByTestId("task-home");
  expect(home).toBeInTheDocument();
});


