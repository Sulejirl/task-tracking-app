import React from "react";
import { useTask } from "contexts/taskContext";
import { Grid } from "@material-ui/core";
import TaskCard from "components/TaskCard";

const Home = () => {
  const { state } = useTask();
  let sortedState = state.tasks.sort(function (a, b) {
    var keyA = new Date(a.createdAt),
      keyB = new Date(b.createdAt);
    // Compare the 2 dates
    if (keyA > keyB) return -1;
    if (keyA < keyB) return 1;
    return 0;
  });
  return (
    <Grid container justifyContent="center" data-testid="task-home">
      {sortedState.map((item, index) => (
        <TaskCard data={item} key={`task-card-${index}`} />
      ))}
    </Grid>
  );
};

export default Home;
