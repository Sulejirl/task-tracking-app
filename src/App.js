import "./App.css";
import Router from "router";
import Header from "components/Header";
import { ThemeProvider } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import MuiTheme from "muiGlobalCss";

function App() {
  return (
    <ThemeProvider theme={MuiTheme}>
      <CssBaseline />
      <div className="App">
        <Header />
        <Router />
      </div>
    </ThemeProvider>
  );
}

export default App;
