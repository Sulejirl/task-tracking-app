import * as React from "react";
const TaksContext = React.createContext();
function taskReducer(state, action) {
  let tempState = { ...state };
  switch (action.type) {
    case "ADD_TASK": {
      let tempTask = { ...action.data };
      tempTask.history = [
        {
          type: "TASK_CREATED",
          date: new Date(),
          oldValue: null,
          newValue: {
            title: tempTask.title,
            description: tempTask.description,
          },
          changedBy: tempTask.createdBy,
        },
      ];
      return { tasks: [...state.tasks, tempTask] };
    }
    case "EDIT_TASK_STATUS": {
      let index = null;
      for (let i = 0; i < tempState.tasks.length; i++) {
        if (tempState.tasks[i].id === action.data.id) {
          index = i;
        }
      }
      if (index !== null) {
        tempState.tasks[index].history = [
          {
            type: "STATUS_CHANGED",
            date: new Date(),
            oldValue: tempState.tasks[index].status,
            newValue: action.data.status,
            changedBy: "John Doe",
          },
          ...(tempState.tasks[index].history || []),
        ];
        tempState.tasks[index].status = action.data.status;
        tempState.tasks[index].updatedAt = new Date();
        tempState.tasks[index].updatedBy = "John Doe";
      }
      return { tasks: [...tempState.tasks] };
    }
    case "EDIT_TASK_ASSIGNED": {
      let index = null;
      for (let i = 0; i < tempState.tasks.length; i++) {
        if (tempState.tasks[i].id === action.data.id) {
          index = i;
        }
      }
      if (index !== null) {
        tempState.tasks[index].history = [
          {
            type: "ASSIGNEE_CHANGED",
            date: new Date(),
            oldValue: tempState.tasks[index].assignedTo,
            newValue: action.data.assignedTo,
            changedBy: "John Doe",
          },
          ...(tempState.tasks[index].history || []),
        ];
        tempState.tasks[index].assignedTo = action.data.assignedTo;
        tempState.tasks[index].updatedAt = new Date();
        tempState.tasks[index].updatedBy = "John Doe";
      }
      return { tasks: [...tempState.tasks] };
    }
    case "EDIT_TASK": {
      let index = null;
      for (let i = 0; i < tempState.tasks.length; i++) {
        if (tempState.tasks[i].id === action.data.id) {
          index = i;
        }
      }
      if (index !== null) {
        let history = [
          {
            type: "TASK_EDITED",
            date: new Date(),
            oldValue: {
              title: tempState.tasks[index].title,
              description: tempState.tasks[index].description,
            },
            newValue: {
              title: action.data.title,
              description: action.data.description,
            },
            changedBy: "John Doe",
          },
          ...(tempState.tasks[index].history || []),
        ];
        tempState.tasks[index] = action.data;
        tempState.tasks[index].history = history;
      }

      return { tasks: [...tempState.tasks] };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}
function TaskProvider({ children, testValue }) {
  const [state, dispatch] = React.useReducer(taskReducer, {
    tasks: testValue || [],
  });
  const value = { state, dispatch };
  return <TaksContext.Provider value={value}>{children}</TaksContext.Provider>;
}
function useTask() {
  const context = React.useContext(TaksContext);
  if (context === undefined) {
    throw new Error("useTask must be used within a TaskProvider");
  }
  return context;
}
export { TaskProvider, useTask };
