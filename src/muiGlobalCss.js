import { createTheme } from '@material-ui/core/styles';

export default createTheme({
  fontFamily: " 'Baloo 2', cursive",

  typography: {
    fontFamily: " 'Baloo 2', cursive",
  },
  overrides: {
    // Style sheet name ⚛️
    MuiFormControl: {
      fullWidth: {
        margin: '5px 0px',
      },
    },
  },
});
