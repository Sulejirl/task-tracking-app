import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  Button,
  TextField,
  Grid,
  IconButton,
} from "@material-ui/core";
import { useForm, Controller } from "react-hook-form";
import { useTask } from "contexts/taskContext";
import { Add, Edit } from "@material-ui/icons";
import PropTypes from "prop-types";

import { v4 as uuidv4 } from "uuid";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    borderRadius: "10px",
  },
  textfield: {
    marginBottom: "24px",
  },
}));

/**
 * Task Modal with form to add or edit tasks
 */
export default function TaskModal({ data }) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const { dispatch } = useTask();

  const {
    handleSubmit,
    reset,
    control,
    formState: { errors },
  } = useForm();
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = (reason) => {
    setOpen(false);
    reset();
  };
  const onSubmit = (fields) => {
    let tempData = {};
    if (data) {
      tempData = {
        ...data,
        ...fields,
        updatedAt: new Date(),
        updatedBy: "Mehmet",
      };
      dispatch({ type: "EDIT_TASK", data: tempData });
    } else {
      tempData = {
        ...fields,
        createdAt: new Date(),
        createdBy: "Mehmet",
        status: "ToDo",
        id: uuidv4(),
      };
      dispatch({ type: "ADD_TASK", data: tempData });
    }
    setOpen(false);
    reset();
  };
  return (
    <div>
      <IconButton
        data-testid="task-modal-button"
        type="button"
        onClick={handleOpen}
      >
        {data ? <Edit /> : <Add />}
      </IconButton>
      <Modal
        className={classes.modal}
        data-testid="task-modal"
        open={open}
        onClose={handleClose}
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 200,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 data-testid="task-modal-title">
              {data ? "Edit " : "Add "} Task
            </h2>
            <form
              onSubmit={handleSubmit(onSubmit)}
              data-testid="task-modal-form"
            >
              <Grid container>
                <Grid item xs={12}>
                  <Controller
                    name="title"
                    control={control}
                    defaultValue={data?.title || ''}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <TextField
                        fullWidth
                        id="title"
                        data-testid="task-modal-title-field"
                        className={classes.textfield}
                        error={!!errors?.title}
                        helperText={
                          !!errors?.title ? "This field is required" : ""
                        }
                        label={"Title"}
                        variant="outlined"
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item xs={12}>
                  <Controller
                    name="description"
                    control={control}
                    defaultValue={data?.description || ''}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <TextField
                        fullWidth
                        id="description"
                        data-testid="task-modal-description-field"
                        className={classes.textfield}
                        variant="outlined"
                        label="Description"
                        multiline
                        rows={4}
                        error={!!errors?.description}
                        helperText={
                          !!errors?.description ? "This field is required" : ""
                        }
                        {...field}
                      />
                    )}
                  />
                </Grid>
                <Grid item container justifyContent="space-around">
                  <Button
                    data-testid="task-modal-close-button"
                    variant="outlined"
                    color="primary"
                    onClick={handleClose}
                  >
                    Close
                  </Button>
                  <Button
                    data-testid="task-modal-save-button"
                    variant="contained"
                    color="primary"
                    type="submit"
                  >
                    {data ? "Edit " : "Add "} Task
                  </Button>
                </Grid>
              </Grid>
            </form>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}

TaskModal.propTypes = {
  /**
   * Edit values of tasks
   */
  data: PropTypes.object,
};
