import { render, screen,fireEvent } from "@testing-library/react";
import TaskModal from "./index";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { TaskProvider } from "contexts/taskContext";

test("Renders Modal Button", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  expect(button).toBeInTheDocument();
});

test("renders modal title 'Add Task'", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  act(() => {
    userEvent.click(button);
  });
  const h2 = screen.getByTestId("task-modal-title");
  expect(h2).toBeInTheDocument();
});
test("Renders 'title' field", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  act(() => {
    userEvent.click(button);
  });
  const field = screen.getByTestId("task-modal-title-field");
  expect(field).toBeInTheDocument();
});

test("Renders 'description' field", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  act(() => {
    userEvent.click(button);
  });
  const field = screen.getByTestId("task-modal-description-field");
  expect(field).toBeInTheDocument();
});

test("Renders 'Close Button' field", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  act(() => {
    userEvent.click(button);
  });
  const closeButton = screen.getByTestId("task-modal-close-button");
  expect(closeButton).toBeInTheDocument();
});

test("Renders 'Save Button' field", () => {
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  act(() => {
    userEvent.click(button);
  });
  const saveButton = screen.getByTestId("task-modal-save-button");
  expect(saveButton).toBeInTheDocument();
});

test("Clicks Close Button", async () => {
  const promise = Promise.resolve();
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  userEvent.click(button);
  const closeButton = screen.getByTestId("task-modal-close-button");
  userEvent.click(closeButton);
  await act(() => promise);
});

test("Create New Task", async () => {
  const promise = Promise.resolve();
  render(
    <TaskProvider>
      <TaskModal />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  userEvent.click(button);

  const titleField = screen.getByTestId("task-modal-title-field");
  const descriptionField = screen.getByTestId("task-modal-description-field");
  fireEvent.input(screen.getByRole("textbox", { name: /description/i }), {
    target: {
      value: "Description"
    }
  });
  fireEvent.input(screen.getByRole("textbox", { name: /title/i }), {
    target: {
      value: "Title"
    }
  });
  const saveButton = screen.getByTestId("task-modal-save-button");
  fireEvent.submit(saveButton);
  await act(() => promise);
});
