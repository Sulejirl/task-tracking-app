import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Modal,
  Backdrop,
  Fade,
  IconButton,
  Typography,
} from "@material-ui/core";
import { History } from "@material-ui/icons";
import moment from "moment";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    maxHeight: "80vh",
    overflow: "scroll",
  },
  typography: {
    width: "20vw",
    [theme.breakpoints.down("sm")]: {
      width: "80vw",
    },
    [theme.breakpoints.only("md")]: {
      width: "30vw",
    },
  },
  typographyDate: {
    display: "flex",
    flexDirection: "row-reverse",
    fontSize: "12px",
    color: " #aaa",
  },
  taskLine: {
    marginBottom: "10px",
    borderBottom: "1px solid grey",
    width: "20vw",
    [theme.breakpoints.down("sm")]: {
      width: "80vw",
    },
    [theme.breakpoints.only("md")]: {
      width: "30vw",
    },
  },
}));

/**
 * History modal component to display task history
 */
const HistoryModal = ({ history }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const createHistory = () => {
    let result = [];
    let index = 0;
    for (let item of history || []) {
      switch (item.type) {
        case "TASK_CREATED": {
          result.push(
            <div className={classes.taskLine} key={`task-history-${index}`}>
              <Typography>
                Task created by <b>{item.changedBy}</b>
              </Typography>
              <Typography className={classes.typographyDate}>
                {moment(item.date).fromNow()}
              </Typography>
            </div>
          );

          break;
        }
        case "TASK_EDITED": {
          if (item.oldValue?.title !== item.newValue?.title) {
            result.push(
              <div className={classes.taskLine} key={`task-history-${index}`}>
                <Typography>
                  Task title changed from <b>{item.oldValue.title}</b> to{" "}
                  <b>{item.newValue.title}</b> by <b>{item.changedBy}</b>
                </Typography>
                <Typography className={classes.typographyDate}>
                  {moment(item.date).fromNow()}
                </Typography>
              </div>
            );
          }
          if (item.oldValue?.description !== item.newValue?.description) {
            result.push(
              <div className={classes.taskLine} key={`task-history-${index}`}>
                <Typography>
                  Task description changed from{" "}
                  <b>{item.oldValue.description}</b> to{" "}
                  <b>{item.newValue.description}</b> by <b>{item.changedBy}</b>
                </Typography>
                <Typography className={classes.typographyDate}>
                  {moment(item.date).fromNow()}
                </Typography>
              </div>
            );
          }

          break;
        }
        case "ASSIGNEE_CHANGED": {
          if (item.oldValue) {
            result.push(
              <div className={classes.taskLine} key={`task-history-${index}`}>
                <Typography className={classes.typography}>
                  Assignee changed from <b>{item.oldValue}</b> to{" "}
                  <b>{item.newValue}</b> by <b>{item.changedBy}</b>
                </Typography>
                <Typography className={classes.typographyDate}>
                  {moment(item.date).fromNow()}
                </Typography>
              </div>
            );
          } else {
            result.push(
              <div className={classes.taskLine} key={`task-history-${index}`}>
                <Typography className={classes.typography}>
                  Task assigned to <b>{item.newValue}</b> by{" "}
                  <b>{item.changedBy}</b>
                </Typography>
                <Typography className={classes.typographyDate}>
                  {moment(item.date).fromNow()}
                </Typography>
              </div>
            );
          }

          break;
        }
        case "STATUS_CHANGED": {
          result.push(
            <div className={classes.taskLine} key={`task-history-${index}`}>
              <Typography className={classes.typography}>
                Status changed from <b>{item.oldValue}</b> to{" "}
                <b> {item.newValue}</b> by <b>{item.changedBy}</b>
              </Typography>
              <Typography className={classes.typographyDate}>
                {moment(item.date).fromNow()}
              </Typography>
            </div>
          );

          break;
        }
        default: {
          break;
        }
      }
      index++;
    }
    return result;
  };
  return (
    <div>
      <IconButton
        onClick={handleOpen}
        data-testid="history-modal-button"
      >
        <History />
      </IconButton>
      <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 200,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">History</h2>
            {createHistory()}
          </div>
        </Fade>
      </Modal>
    </div>
  );
};

HistoryModal.propTypes = {
  /**
   * Array of object for keep track of the task history
   */
  history: PropTypes.array.isRequired,
};
export default HistoryModal;
