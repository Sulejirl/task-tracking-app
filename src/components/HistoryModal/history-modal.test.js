import { render, screen } from "@testing-library/react";
import HistoryModal from "./index";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { TaskProvider } from "contexts/taskContext";
const data = [
  {
    changedBy: "Mehmet",
    date: new Date(),
    newValue: "Müller",
    oldValue: undefined,
    type: "ASSIGNEE_CHANGED",
  },
  {
    changedBy: "Mehmet",
    date: new Date(),
    newValue: "InProgress",
    oldValue: "ToDo",
    type: "STATUS_CHANGED",
  },
  {
    changedBy: "Mehmet",
    date: new Date(),
    newValue: { title: "we", description: "qwe" },
    oldValue: null,
    type: "TASK_CREATED",
  },
  {
    changedBy: "Mehmet",
    date: new Date(),
    newValue: { title: "asd", description: "dasd" },
    oldValue: { title: "we", description: "qwe" },
    type: "TASK_EDITED",
  },
  {
    type: "DEFAULT",
  },
];

test("Renders Modal Button", () => {
  render(
    <TaskProvider>
      <HistoryModal history={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("history-modal-button");
  expect(button).toBeInTheDocument();
});

test("Renders with props", () => {
  render(
    <TaskProvider>
      <HistoryModal history={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("history-modal-button");
  userEvent.click(button);
  const title = screen.getByText(/History/i);
  expect(title).toBeInTheDocument();
});

test("Renders ASSIGNEE_CHANGED sentence", () => {
  render(
    <TaskProvider>
      <HistoryModal history={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("history-modal-button");
  userEvent.click(button);
  const title = screen.getByText(/Task assigned to/i);
  expect(title).toBeInTheDocument();
});
