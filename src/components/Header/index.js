import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import AddTask from "components/TaskModal";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  toolbar: {
    display: "flex",
    justifyContent: "space-between",
  },
}));
/**
 * App header to display app name and add new tasks
 */
const Header = () => {
  const classes = useStyles();
  return (
      <AppBar position="sticky">
        <Toolbar className={classes.toolbar}>
          <Typography className={classes.title} variant="h6">
            Task Tracker
          </Typography>
          <AddTask />
        </Toolbar>
      </AppBar>
  );
};

export default Header;
