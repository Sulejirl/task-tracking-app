import { render, screen } from "@testing-library/react";
import Header from "./index";
import { TaskProvider } from "contexts/taskContext";

test("Renders 'Task Tracker' at the header", () => {
  render(
    <TaskProvider>
      <Header />
    </TaskProvider>
  );
  const linkElement = screen.getByText(/Task Tracker/i);
  expect(linkElement).toBeInTheDocument();
});

