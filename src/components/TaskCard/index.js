import React from "react";
import {
  Grid,
  Card,
  CardContent,
  CardHeader,
  CardActions,
  Chip,
  IconButton,
  Divider,
  Menu,
  MenuItem,
  Avatar,
  Typography,
} from "@material-ui/core";
import { Person } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core/styles";
import { useTask } from "contexts/taskContext";
import EditTask from "components/TaskModal";
import History from "components/HistoryModal";
import PropTypes from "prop-types";
import moment from "moment";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: "8px",
    width: "20vw",
    [theme.breakpoints.down("sm")]: {
      width: "95vw",
    },
    [theme.breakpoints.only("md")]: {
      width: "30vw",
    },
  },
  cardContent: {
    overflowWrap: "break-word",
  },
  cardActions: {
    display: "flex",
    justifyContent: "space-between",
  },
  chip: {
    fontWeight: "bold",
  },
  header: {
    display: "flex",
  },
  titleTypographyProps: {
    width: "8vw",
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
    [theme.breakpoints.down("sm")]: {
      width: "50vw",
    },
    [theme.breakpoints.only("md")]: {
      width: "15vw",
    },
  },
  subInfo: {
    fontSize: "12px",
    color: "#aaa",
  },
}));

const people = [
  "Ahmet",
  "Thomas",
  "Müller",
  "Neuer",
  "Manuel",
  "Testoglu",
  "John Doe",
];

const stateTransitions = {
  ToDo: ["InProgress"],
  InProgress: ["InQA", "Blocked"],
  Blocked: ["ToDo"],
  InQA: ["ToDo", "Done"],
  Done: ["Deployed"],
  Deployed: [],
};
const stateColors = {
  ToDo: "lightgreen",
  InProgress: "yellow",
  Blocked: "red",
  InQA: "brown",
  Done: "green",
  Deployed: "grey",
};

/**
 * Task Card component to display task values
 * User can change Status
 * User can change Assignee
 * User can edit title and description of task
 */
const TaskCard = ({ data }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorElAssignedTo, setAnchorElAssignedTo] = React.useState(null);
  const { dispatch } = useTask();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleChange = (value) => {
    let editData = { id: data.id, status: value };
    setAnchorEl(null);
    dispatch({ type: "EDIT_TASK_STATUS", data: editData });
  };
  const handleAssignedChange = (value) => {
    let editData = { id: data.id, assignedTo: value };
    setAnchorElAssignedTo(null);
    dispatch({ type: "EDIT_TASK_ASSIGNED", data: editData });
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <>
            <Chip
              data-testid="task-card-status-chip"
              aria-label="recipe"
              label={data.status}
              className={classes.chip}
              style={{ backgroundColor: stateColors[data.status] }}
              onClick={data.status !== "Deployed" ? handleClick : null}
            />
            <Menu
              data-testid="task-card-status-menu"
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              transitionDuration={1}
              onClose={handleClose}
            >
              {stateTransitions[data.status]?.map((item, index) => (
                <MenuItem
                  key={`task-card-status-menu-item-${index}`}
                  onClick={() => handleChange(item)}
                >
                  {item}
                </MenuItem>
              ))}
            </Menu>
          </>
        }
        action={data.status === "Deployed" ? null : <EditTask data={data} />}
        title={data.title}
        titleTypographyProps={{ className: classes.titleTypographyProps }}
        className={classes.header}
        // subheader={`Created At: ${moment(data.createdAt).format("DD.MM.YYYY")}`}
      />
      <Divider />
      <CardContent className={classes.cardContent}>
        <Grid container alignContent="space-between">
          <Grid item container xs={12} justifyContent="center">
            <Typography>{data.description}</Typography>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions className={classes.cardActions}>
        <Grid container>
          <Grid item container xs={12} justifyContent="space-between" alignItems = "center">
            <>
              {data.assignedTo ? (
                <IconButton
                  data-testid="task-card-assignee-avatar"
                  onClick={
                    data.status !== "Deployed"
                      ? (e) => setAnchorElAssignedTo(e.currentTarget)
                      : null
                  }
                >
                  <Avatar aria-label="recipe" className={classes.avatar}>
                    {data.assignedTo[0]}
                  </Avatar>
                </IconButton>
              ) : (
                <IconButton
                  data-testid="task-card-assignee-icon"
                  onClick={
                    data.status !== "Deployed"
                      ? (e) => setAnchorElAssignedTo(e.currentTarget)
                      : null
                  }
                >
                  <Person />
                </IconButton>
              )}
              <Menu
                data-testid="task-card-assignee-menu"
                id="simple-menu"
                anchorEl={anchorElAssignedTo}
                keepMounted
                open={Boolean(anchorElAssignedTo)}
                transitionDuration={1}
                onClose={() => setAnchorElAssignedTo(null)}
              >
                {people?.map((item, index) => (
                  <MenuItem
                    key={`task-card-assignee-menu-item-${index}`}
                    onClick={() => handleAssignedChange(item)}
                  >
                    {item}
                  </MenuItem>
                ))}
              </Menu>
            </>
            <History history={data.history} />
          </Grid>
          <Grid item container xs={12} justifyContent="space-between">
            <Grid item>
              <Typography className={classes.subInfo}>
                Created By: {data.createdBy}
              </Typography>
            </Grid>
            <Grid item>
              <Typography className={classes.subInfo}>
                Created At: {moment(data.createdAt).format("LLLL")}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  );
};
TaskCard.propTypes = {
  /**
   * Data to display at the task card
   */
  data: PropTypes.object.isRequired,
};

export default TaskCard;
