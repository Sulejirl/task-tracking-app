import { render, screen,fireEvent } from "@testing-library/react";
import TaskCard from "./index";
import userEvent from "@testing-library/user-event";
import { act } from "react-dom/test-utils";
import { TaskProvider } from "contexts/taskContext";

test("Renders with props", () => {
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "asdasd",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "asdasd",
  };
  render(
    <TaskProvider>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-status-chip");
  expect(button).toBeInTheDocument();
});
test("Renders title", () => {
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "description",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "Lorem Ipsum",
  };
  render(
    <TaskProvider>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const title = screen.getByText(/Lorem Ipsum/i);
  expect(title).toBeInTheDocument();
});
test("Renders description", () => {
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "Lorem Ipsum",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "title",
  };
  render(
    <TaskProvider>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const title = screen.getByText(/Lorem Ipsum/i);
  expect(title).toBeInTheDocument();
});
test("Renders Status Menu Item", () => {
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "asdasd",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "asdasd",
  };
  render(
    <TaskProvider>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-status-chip");
  userEvent.click(button);
  const menuItem = screen.getByText(/InProgress/i);
  expect(menuItem).toBeInTheDocument();
});

test("Click on Status Menu Item", async () => {
  const promise = Promise.resolve();
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "Lorem Ipsum",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "title",
  };
  render(
    <TaskProvider testValue= {[data]}>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-status-chip");
  userEvent.click(button);
  const menuItem = screen.getByText(/InProgress/i);
  fireEvent.click(menuItem);
  await act(() => promise);
});

test("Renders Avatar Menu Item", () => {
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "asdasd",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "asdasd",
    assignedTo: "Testoglu",
  };
  render(
    <TaskProvider>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-assignee-avatar");
  userEvent.click(button);
  const menuItem = screen.getByText(/Ahmet/i);
  expect(menuItem).toBeInTheDocument();
});

test("Click on Assignee Menu Item", async () => {
  const promise = Promise.resolve();
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "Lorem Ipsum",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "title",
    assignedTo: "Testoglu",
  };
  render(
    <TaskProvider testValue= {[data]}>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-assignee-avatar");
  userEvent.click(button);
  const menuItem = screen.getByText(/Ahmet/i);
  userEvent.click(menuItem);
  await act(() => promise);
});
test("Assign task to a person", async () => {
  const promise = Promise.resolve();
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "Lorem Ipsum",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "title",
  };
  render(
    <TaskProvider testValue= {[data]}>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-card-assignee-icon");
  userEvent.click(button);
  const menuItem = screen.getByText(/Ahmet/i);
  userEvent.click(menuItem);
  await act(() => promise);
});
test("Edit a Task", async () => {
  const promise = Promise.resolve();
  const data = {
    createdAt: new Date(),
    createdBy: "Mehmet",
    description: "Lorem Ipsum",
    history: [
      {
        changedBy: "Mehmet",
        date: new Date(),
        newValue: "Neuer",
        oldValue: undefined,
        type: "ASSIGNEE_CHANGED",
      },
    ],
    id: "5792e75b-a73e-40fe-a08f-c28692f35a5b",
    status: "ToDo",
    title: "title",
  };
  render(
    <TaskProvider testValue= {[data]}>
      <TaskCard data={data} />
    </TaskProvider>
  );
  const button = screen.getByTestId("task-modal-button");
  userEvent.click(button);
  fireEvent.input(screen.getByRole("textbox", { name: /description/i }), {
    target: {
      value: "Description"
    }
  });
  fireEvent.input(screen.getByRole("textbox", { name: /title/i }), {
    target: {
      value: "Title"
    }
  });
  const saveButton = screen.getByTestId("task-modal-save-button");
  fireEvent.submit(saveButton);
  await act(() => promise);
});
